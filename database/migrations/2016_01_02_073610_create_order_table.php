<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_no',15);
            $table->datetime('order_date');
            $table->integer('user_id');
            $table->integer('order_type');
            $table->integer('cart_id');
            $table->integer('delivery_method_type')->nullable();
            $table->integer('delivery_id')->nullable();
            $table->integer('preffered_delivery_time_id')->nullable();
            $table->double('net_price');
            $table->double('tax_amount');
            $table->double('discount_amount');
            $table->double('grand_total');
            $table->integer('coupon_id')->nullable();
            $table->integer('payment_option_type');
            $table->integer('order_status_type');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->datetime('created_at');
            $table->datetime('updated_at');
            $table->datetime('deleted_at')->nullable();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order');
    }
}
