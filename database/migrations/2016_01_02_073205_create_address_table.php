<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number',5);
            $table->string('street',255);
            $table->string('locality',255);
            $table->string('city',100);
            $table->string('state',50);
            $table->string('country',50);
            $table->string('pin',10);
            $table->double('latitude');
            $table->double('longitude');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->datetime('created_at');
            $table->datetime('updated_at');
            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('address');
    }
}